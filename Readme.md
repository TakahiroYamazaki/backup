# 画面説明

![メイン画面](image/main.png "main")

* backupボタン：バックアップを開始する
* settingボタン：バックアップのセッティングを行う

# 設定画面
![設定画面](image/config.png "config")

# 設定方法
![設定ファイル](image/file.png "file")

同一ディレクトリにconfig.jsonを置いてバックアップ内容を記載する。

* CopyList：コピーをするディレクトリを指定する
* ExcludeList：現在未対応
* OutputPath：出力先のディレクトリを指定する。

# 特記事項
1. 環境変数が使える
1. ファイル指定も可能
1. *.xmlには対応しておいた