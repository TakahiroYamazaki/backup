﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utills;
using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.IO.Compression;
namespace Utills.Tests
{
    [TestClass()]
    public class DirectoryBackupTests
    {
        readonly string TestDirectory = "root";
        readonly string OutDirectory = "out";
        readonly string ZipDir = "test.zip";
        readonly string ListDir = "list";
        readonly string SubDirectory = "sub";

        readonly string TestFile = "nanka.txt";
        readonly string ZipFilePath = "test.zip";

        [TestInitialize()]
        public void IntialTest()
        {
            var dirs = new List<string>(){
                OutDirectory,TestDirectory, ZipDir
            };

            foreach (var dir in dirs)
            {
                if (Directory.Exists(dir))
                {
                    DeleteDirectory(dir);
                }
            }
            if (File.Exists(ZipFilePath))
            {
                File.Delete(ZipFilePath);
            }
            //  ディレクトリ作成
            Directory.CreateDirectory(TestDirectory);
            var fs = File.Create(Path.Combine(TestDirectory, TestFile));
            fs.Close();
            Directory.CreateDirectory(Path.Combine(TestDirectory, SubDirectory));
            Directory.CreateDirectory(TestDirectory + "2");
            fs = File.Create(Path.Combine(TestDirectory + "2", TestFile + "2"));
            fs.Close();
        }
        public void DeleteDirectory(string path)
        {
            if (Directory.Exists(path))
            {
                var info = new DirectoryInfo(path);
                foreach (var f in info.GetFiles())
                {
                    File.Delete(f.FullName);
                }
                foreach (var f in info.GetDirectories())
                {
                    Directory.Delete(f.FullName);
                }
                Directory.Delete(path);
            }
        }

        [TestMethod()]
        public void DirCopyTest()
        {
            var obj = new DirectoryBackup();
            obj.DirCopy(TestDirectory, OutDirectory);

            if (false == File.Exists(Path.Combine(OutDirectory, TestFile)))
            {
                Assert.Fail("error");
            }
        }

        [TestMethod()]
        public void CopyWithZipTest()
        {
            var obj = new DirectoryBackup();
            obj.CopyWithZip(TestDirectory, ZipFilePath);
            if (!File.Exists(ZipFilePath))
            {
                Assert.Fail("error");
            }
            using (ZipArchive a = ZipFile.OpenRead(ZipFilePath))
            {
                foreach (var o in a.Entries)
                {
                    if (o.FullName.Contains(TestFile))
                    {
                        return;
                    }
                }
            }


            Assert.Fail("no archive");
        }



        [TestMethod()]
        public void CopyListTest()
        {
            Assert.Fail("1 error");
            var obj = new DirectoryBackup();
            var list = new List<string>(){
                TestDirectory,
                TestDirectory+"2",
            };
            obj.CopyList(list, ListDir);
            if (!File.Exists(Path.Combine(TestDirectory, TestFile)))
            {

                Assert.Fail("1 error");
            }
            if (!File.Exists(Path.Combine(TestDirectory + "2", TestFile + "2")))
            {
                Assert.Fail("2 error");
            }
        }

        [TestMethod()]
        public void GetDirctoryNameTest()
        {
            var obj = new DirectoryBackup();
            Assert.AreEqual(obj.AsDynamic().GetDirctoryName("d\\hoge\\"), "hoge");
            Assert.AreEqual(obj.AsDynamic().GetDirctoryName("d\\hoge"), "hoge");

        }
        [TestMethod()]
        public void ExchangeEnvironmentTest()
        {
            var obj = new DirectoryBackup();
            Assert.AreEqual(obj.AsDynamic().ExchangeEnvironment("%APPDATA%"), System.Environment.ExpandEnvironmentVariables("%APPDATA%"));
            Assert.AreEqual(obj.AsDynamic().ExchangeEnvironment("aho%APPDATA%"), "aho" + System.Environment.ExpandEnvironmentVariables("%APPDATA%"));
        }

        [TestMethod()]
        public void FileCopyTest()
        {
            string root = @"filetest";
            DeleteDirectory(root);
            var fs = File.Create(TestFile + ".xml");
            fs.Close();

            var obj = new DirectoryBackup();
            obj.FileCopy("../*.xml", OutDirectory);
            if (false == File.Exists(Path.Combine(OutDirectory, TestFile + ".xml")))
            {
                Assert.Fail("error");
            }
        }
    }
}
