﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utills;
using Microsoft.VisualStudio.TestTools.UnitTesting;
namespace Utills.Tests
{
    
    [System.Runtime.Serialization.DataContract, Serializable]
    class TestClass
    {

        [System.Runtime.Serialization.DataMember()]
       public  string DataName = "micro";
        [System.Runtime.Serialization.DataMember()]
        public string Kind = "";
        [System.Runtime.Serialization.DataMember()]
        public List<string> Array = new List<string>(){
            "ee","maka"
        };

        public override bool Equals(object obj)
        {
            if (obj.GetType() != this.GetType())
            {
                return false;
            }
            var input = (TestClass)obj;
            if (DataName != input.DataName)
            {
                return false;
            }
            if (Kind != input.Kind)
            {
                return false;
            }
            if (false == Array.SequenceEqual(input.Array))
            {
                return false;
            }
            return true;
        }
        public override int GetHashCode()
        {
            return DataName.GetHashCode() ^ Kind.GetHashCode();
        }
    }
    [TestClass()]
    public class JsonSerializerTests
    {
        readonly string TestFilePath = "writetest.json";
        /// <summary>
        /// オブジェクトがwriteの場合の変換
        /// </summary>
        [TestMethod()]
        public void ReadWriteTest()
        {
            var dataname = "else";
            var kind = "nemui;";
            var list = new List<string>() { "koko", "kara" };
            var input = new TestClass()
            {
                DataName = dataname,
                Kind = kind,
                Array = list

            };
            var obj = new JsonSerializer();
            obj.Write<TestClass>(input, TestFilePath);
            if (false == System.IO.File.Exists(TestFilePath))
            {
                Assert.Fail("file create error");
            }
            var output = obj.Read<TestClass>(TestFilePath);
            Assert.AreEqual(input.Equals(output), true);
        }
        /// <summary>
        /// オブジェクトがリストの場合の返還
        /// </summary>
        [TestMethod()]
        public void ListTest()
        {
            var input = new List<TestClass>()
            {
                new TestClass(),
                new TestClass()
            };
            var obj = new JsonSerializer();
            obj.Write<List<TestClass>>(input, TestFilePath);
            if (false == System.IO.File.Exists(TestFilePath))
            {
                Assert.Fail("file create error");
            }
            var output = obj.Read<List<TestClass>>(TestFilePath);
            Assert.AreEqual(input.Count, output.Count);
        }
    }
}
