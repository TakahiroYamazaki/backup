﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DirBackup.Config
{
    
    [System.Runtime.Serialization.DataContract, Serializable]
    public class ConfigJson : Utills.BindBase
    {
        List<string> copyList;
        [System.Runtime.Serialization.DataMember()]
        public List<string> CopyList
        {
            get { return copyList; }
            set { SetProperty(ref copyList, value); }
        }
        [System.Runtime.Serialization.DataMember()]
        public List<string> ExcludeList;
 
        string outputPath;
        [System.Runtime.Serialization.DataMember()]
        public string OutputPath
        {
            get { return outputPath; }
            set { SetProperty(ref outputPath, value); }
        }
    }
}
