﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DirBackup
{
    /// <summary>
    /// MainWindow.xaml の相互作用ロジック
    /// </summary>
    public partial class MainWindow : Window
    {
        readonly string ConfigFilePath = "config.json";
        Config.ConfigJson jsonFile;
        Helper.DirBackup backup = new Helper.DirBackup();
        public MainWindow()
        {
            InitializeComponent();
            var jsonparser = new Utills.JsonSerializer();
            jsonFile = jsonparser.Read<Config.ConfigJson>(ConfigFilePath);
        }

        private void Config_Click(object sender, RoutedEventArgs e)
        {
            var dlg = new View.ConfigView(jsonFile);
            dlg.ShowDialog();
        }

        private async void Backup_Click(object sender, RoutedEventArgs e)
        {
            BtnBack.IsEnabled = false;
            var result = await backup.RunTaskSync(jsonFile);
            if (result == "")
            {
                MessageBox.Show("正常終了");
            }
            else
            {
                //  Todoエラーをわかりやすくする
                MessageBox.Show(result);
            }
            BtnBack.IsEnabled = true;
        }
    }
}
