﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DirBackup.Helper
{
    class DirBackup
    {
        public Task<string> RunTaskSync(Config.ConfigJson configFile)
        {
            return Task.Run<string>(() => Run(configFile));
        }
        public string Run(Config.ConfigJson configFile)
        {
            //  ディレクトリのコピー
            var obj = new Utills.DirectoryBackup();
            var result = "";
            try
            {
                result = obj.CopyList(configFile.CopyList, configFile.OutputPath);
            }
            catch(Exception e)
            {
                result = e.ToString();
            }
            return result;
        }
    }
}
