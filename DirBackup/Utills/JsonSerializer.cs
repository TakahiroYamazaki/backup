﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization.Json;
using System.IO;

namespace Utills
{
    /// <summary>
    /// Jsonのシリアライズ用クラス
    /// </summary>
    public class JsonSerializer
    {
        /// <summary>
        /// クラスをシリアライズして読み込む
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="path"></param>
        /// <returns></returns>
        public T Read<T>(string path)
        {
            DataContractJsonSerializer serializer = new DataContractJsonSerializer(typeof(T));
            T obj = default(T);
            using (FileStream fs = new FileStream(path, FileMode.Open, FileAccess.Read))
            {
                obj = (T)serializer.ReadObject(fs);
            }
            return obj;
        }
        /// <summary>
        /// クラスをシリアライズして書き込む
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="obj"></param>
        /// <param name="writePath"></param>
        public void Write<T>(T obj, string writePath)
        {
            DataContractJsonSerializer serializer = new DataContractJsonSerializer(typeof(T));
            using (FileStream fs = new FileStream(writePath, FileMode.Create))
            {
                serializer.WriteObject(fs, obj);
            }        
        }

    }
}
