﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading.Tasks;
using System.IO.Compression;
using System.Text.RegularExpressions;

namespace Utills
{
    public class DirectoryBackup
    {
        /// <summary>
        /// ディレクトリコピーを再帰的に行う
        /// </summary>
        /// <param name="from"></param>
        /// <param name="to"></param>
        public void DirCopy(string from, string to)
        {
            DirectoryInfo dir = new DirectoryInfo(ExchangeEnvironment(from));
            CreateDirectoryIfNeed(to);
            //  ファイルコピー
            foreach (FileInfo file in dir.GetFiles())
            {
                file.CopyTo(Path.Combine(to, file.Name), true);
            }
            //  ディレクトリコピー
            foreach (DirectoryInfo subdir in dir.GetDirectories())
            {
                DirCopy(subdir.FullName, Path.Combine(to, subdir.Name));
            }
        }

        /// <summary>
        /// ファイルのコピーを実施する
        /// </summary>
        /// <param name="from"></param>
        /// <param name="to"></param>
        /// <returns></returns>
        public void FileCopy(string from, string to)
        {
            var dirname = Path.GetDirectoryName(from);
            var filename = Path.GetFileName(from);
            
            CreateDirectoryIfNeed(to);
            DirectoryInfo dir = new DirectoryInfo(ExchangeEnvironment(dirname));
            foreach (var file in dir.GetFiles(filename, SearchOption.AllDirectories))
            {
                file.CopyTo(Path.Combine(to, file.Name), true);
            }
            return;
        }

        public string CopyWithZip(string from, string to)
        {
            ZipFile.CreateFromDirectory(from, to);
            return "error";
        }
        /// <summary>
        /// 指定されたリストのディレクトリコピーを行う。
        /// </summary>
        /// <param name="list"></param>
        /// <param name="to"></param>
        /// <returns></returns>
        public string CopyList(List<string> list, string to)
        {
            CreateDirectoryIfNeed(to);
            foreach (var content in list)
            {
                var path = ExchangeEnvironment(content);
                if (File.Exists(path) || content.Contains("*"))
                {
                    FileCopy(path, to);
                }
                else if (Directory.Exists(path))
                {
                    var dirname = GetDirctoryName(path);
                    DirCopy(content, Path.Combine(to, dirname));
                }

            }
            return "";
        }

        public string CopyListWithZip(List<string> list, string to)
        {
            return "error";
        }

        public void CreateDirectoryIfNeed(string to)
        {
            var dstInfo = new DirectoryInfo(to);
            //  コピー先が存在しない
            if (!dstInfo.Exists)
            {
                dstInfo.Create();
            }
        }
        /// <summary>
        /// ディレクトリ名称を取得する
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        string GetDirctoryName(string path)
        {
            if (path[path.Length - 1] == '\\')
            {
                path = path.Substring(0, path.Length - 1);
            }
            var result = "";
            var idx = path.LastIndexOf("\\");
            if (idx != -1)
            {
                result = path.Substring(idx+1, path.Length - (1+idx));
            }
            return result;
        }
        static Regex splitter = new Regex("%.*%", RegexOptions.Compiled);
        /// <summary>
        /// 環境変数の変換を行う
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        string ExchangeEnvironment(string path)
        {
            string result = path;
            foreach(var val in splitter.Matches(path)){
                var name = System.Environment.ExpandEnvironmentVariables(val.ToString());
                result = result.Replace(val.ToString(), name);
            }
            return result;
        }

        /// <summary>
        /// 出力フォルダがなければ作る
        /// </summary>
        /// <param name="dstInfo"></param>
        void createOutDirIfNeed(DirectoryInfo dstInfo)
        {
            //  コピー先が存在しない
            if (!dstInfo.Exists)
            {
                dstInfo.Create();
            }
        }
    }
}
